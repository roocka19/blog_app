# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user1 = User.create(name: 'Raoul', email:'raoul@yahoo.com')
articles1 = user.articles1.create(title: 'Ruby', content:'Build application with rubyonrails')
comment1 = articles.comment1.create(body:'Learne more about this')

user2 = User.create(name: 'Raoul', email:'raoul@yahoo.com')
articles2 = user.articles.create(title: 'Ruby', content:'Build application with rubyonrails')
comment2 = articles.comment.create(body:'Learne more about this')

user3 = User.create(name: 'Raoul', email:'raoul@yahoo.com')
articles3 = user.articles.create(title: 'Ruby', content:'Build application with rubyonrails')
comment3 = articles.comment.create(body:'Learne more about this')

user4 = User.create(name: 'Raoul', email:'raoul@yahoo.com')
articles4 = user.articles.create(title: 'Ruby', content:'Build application with rubyonrails')
comment4 = articles.comment.create(body:'Learne more about this')

user5 = User.create(name: 'Raoul', email:'raoul@yahoo.com')
articles5 = user.articles.create(title: 'Ruby', content:'Build application with rubyonrails')
comment5 = articles.comment.create(body:'Learne more about this')
